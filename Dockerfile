FROM juniorjbn/java8tomcat7:0.1

RUN cd /opt/apache-tomcat-7.0.88/webapps/ && git clone https://juniorjbn@bitbucket.org/juniorjbn/jsp-demo.git uol

RUN chown -R 1001:1001 /opt/apache-tomcat-7.0.88

USER 1001
RUN touch /opt/apache-tomcat-7.0.88/logs/catalina.out
RUN chmod 777 /opt/apache-tomcat-7.0.88/logs /opt/apache-tomcat-7.0.88/work
RUN chmod 666 /opt/apache-tomcat-7.0.88/logs/catalina.out /opt/apache-tomcat-7.0.88/conf/*

EXPOSE 8080

CMD /opt/apache-tomcat-7.0.88/bin/startup.sh && tail -f /opt/apache-tomcat-7.0.88/logs/catalina.out
